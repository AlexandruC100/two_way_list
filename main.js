// UI Variables
const UIInputField = document.querySelector("#input-field");
const UISelectList = document.querySelector("#select-list");
const UIForm = document.querySelector("#form");
const UIPrintBtn = document.querySelector("#print-btn");
const UIAliveList = document.querySelector("#alive-list");
const UIDeadList = document.querySelector("#dead-list");
const UIList = document.querySelector("#list");

// Add item to list
function addItemToList() {
  if (UISelectList.value === "alive") {
    UIAliveList.innerHTML += `
      <p class="item">${UIInputField.value}</p>
    `;
  } else if (UISelectList.value === "dead") {
    UIDeadList.innerHTML += `
    <p class="item">${UIInputField.value}</p>
    `;
  }
}

// Listen for submit event to add input value to list
UIForm.addEventListener("submit", (e) => {
  addItemToList();

  UIInputField.value = "";

  UIInputField.focus();

  e.preventDefault();
});

// Listen for load event to focus on input element
window.addEventListener("DOMContentLoaded", (e) => {
  UIInputField.focus();

  e.preventDefault();
});

// Listen on click event to print list
UIPrintBtn.addEventListener("click", (e) => {
  UIForm.style.display = "none";

  print();

  setTimeout(() => {
    UIForm.style.display = "block";
  }, 1);

  e.preventDefault();
});

// Listen for click event on item to delete it
UIList.addEventListener("click", (e) => {
  if (e.target.classList.contains("item")) {
    e.target.remove();
  }
});
